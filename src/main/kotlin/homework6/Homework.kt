package homework6

fun main() {
    val myLion = Lion("Артур", 123, 77)
    val myTiger = Tiger("Tigra", 190, 150)
    val myHippo = Hippopotamus("Hippo", 150, 300)
    val myWolf = Wolf("Secret", 170, 100)
    val myGiraffe = Giraffe("Melman", 250, 100)
    val myElephant = Elephant("Elly", 220, 700)
    val myChimpanzee = Chimpanzee("Chico", 150, 70)
    val myGorilla = Gorilla("Hocco", 185, 110)

    // закомментил вызов eat() для каждого животного, чтобы не перегружать консоль
//    myLion.eat("meat")
//    myTiger.eat("milk")
//    myHippo.eat("grass")
//    myWolf.eat("rabbit")
//    myGiraffe.eat("potato")
//    myElephant.eat("grass")
//    myChimpanzee.eat("banana")
//    myGorilla.eat("banana")

    // добавил возможность расширить список предпочтений животного
    println("\nДобавил возможность расширить список предпочтений животного")
    myLion.eat("cookie") // лев Артур не любит печенки
    myLion.addNewFood("cookie") // добавим в список предпочтений
    myLion.addNewFood("cookie") // добавить два раза нельзя
    println(myLion.foodPrefs) // печенки появились в массиве
    myLion.eat("cookie") // теперь лев Артур любит печенки
    println()


    println("=== Задача 2, кормим животных с помощью полиморфизма ===")
    val animals: Array<Animal> = arrayOf(
        myLion,
        myTiger,
        myHippo,
        myWolf,
        myGiraffe,
        myElephant,
        myChimpanzee,
        myGorilla
    )
    val foods = listOf("milk", "potato", "grass")

    eater(animals, foods)

}

// Задача 2
// попытаемся покормить каждое животное каждым продуктом
// перебираем животных и предлагаем каждый продукт
// можно доработать так, чтобы eater() не предлагал еду, которую животное точно не ест
fun eater(animals: Array<Animal>, foodList: List<String>) {
    for (animal in animals) {
        for (food in foodList) {
            animal.eat(food)
        }
    }
}

abstract class Animal() {
    abstract val animalType: String //добавил значение типа животного
    abstract val name: String
    abstract var height: Int
    abstract var weight: Int
    abstract var foodPrefs: MutableList<String> // изменяемый список под предпочтения

    //сытость по умолчанию 0, переодпределять ее в других классах не нужно
    //сделал fullness protected, так как переменная относится к внутренней логике класса
    //дописать функцию в субклассе, которая будет взаимодействовать с сытостью, можно
    protected var fullness: Int = 0 //сытость по умолчанию 0, переодпределять ее в других классах не нужно

    //вынес проверку на наличие еды в предпочтениях, так как пользуюсь этим в двух функциях
    fun isFavFood(food: String): Boolean {
        var favFood = false
        for (i in foodPrefs) {
            if (food == i) {
                favFood = true
            }
        }
        return favFood
    }

    // Функция eat имеет одинаковую реализацию для всех животных, поэтому определена однозначно
    fun eat(food: String) {
        if (isFavFood(food)) {
            fullness++
            println("$animalType $name съёл $food, сытость равна $fullness")
        }
        else println("$animalType $name не любит $food")
    }

    //добавил функцию, которая добавляет еду в список предпочтений
    fun addNewFood (food: String) {
        if (isFavFood(food)) println("$food уже есть в списке любимой еды $name")
        else {
            foodPrefs.add(food)
            println("$food добавлен в список любимой еды $name")
        }
    }
}

class Lion(override val name: String, override var height: Int, override var weight: Int): Animal() {
    override val animalType: String = "Лев" //задаю вид животного
    override var foodPrefs: MutableList<String> = mutableListOf("meat", "milk")
}

class Tiger(override val name: String, override var height: Int, override var weight: Int): Animal() {
    override val animalType: String = "Тигр" //задаю вид животного
    override var foodPrefs: MutableList<String> = mutableListOf("meat", "milk", "pork")
}

class Hippopotamus(override val name: String, override var height: Int, override var weight: Int): Animal() {
    override val animalType: String = "Бегемот" //задаю вид животного
    override var foodPrefs: MutableList<String> = mutableListOf("grass", "milk")
}

class Wolf(override val name: String, override var height: Int, override var weight: Int): Animal() {
    override val animalType: String = "Волк" //задаю вид животного
    override var foodPrefs: MutableList<String> = mutableListOf("meat", "rabbit")
}

class Giraffe(override val name: String, override var height: Int, override var weight: Int): Animal() {
    override val animalType: String = "Жираф" //задаю вид животного
    override var foodPrefs: MutableList<String> = mutableListOf("grass")
}

class Elephant(override val name: String, override var height: Int, override var weight: Int): Animal() {
    override val animalType: String = "Слон" //задаю вид животного
    override var foodPrefs: MutableList<String> = mutableListOf("grass")
}

class Chimpanzee(override val name: String, override var height: Int, override var weight: Int): Animal() {
    override val animalType: String = "Шимпанзе" //задаю вид животного
    override var foodPrefs: MutableList<String> = mutableListOf("banana")
}

class Gorilla(override val name: String, override var height: Int, override var weight: Int): Animal() {
    override val animalType: String = "Горилла" //задаю вид животного
    override var foodPrefs: MutableList<String> = mutableListOf("banana")
}