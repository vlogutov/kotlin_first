package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)

    //завожу итераторы для подсчета учеников
    //использую Double.
    // Хотел взять float (так как его точности достаточно), но неожиданно получил длинную дробную часть
    var fivePupils = 0.0
    var fourPupils = 0.0
    var threePupils = 0.0
    var twoPupils = 0.0

    //перебор элементов массива и подсчет учеников разной успеваемости
    for (item in marks) {
        when (item) {
            5 -> fivePupils++
            4 -> fourPupils++
            3 -> threePupils++
            2 -> twoPupils++
        }
    }

    //вывод информации, доля учеников подсчитывается внутри println
    println("Отличников - " + (fivePupils / marks.size * 100) + "%")
    println("Хорошистов - " + (fourPupils / marks.size * 100) + "%")
    println("Троечников - " + (threePupils / marks.size * 100) + "%")
    println("Двоечников - " + (twoPupils / marks.size * 100) + "%")
}