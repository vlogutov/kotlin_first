package homework4

/*
Разобрался, почему в первом циле for используется -2
Если поставить -1, то в последнюю итерацию первого цикла for i будет = 8
В таком случае во втором цикле for правое условие станет myArray.size(9) - 1 - i(8) = 0
То есть j должен быть одновременно и равен и не равен 0. Захода в цикл не будет, лишняя итерация

Можно в первом for поменять условие на for(i in 0 until myArray.size - 1), будет равнозначно
 */

fun main() {
    val myArray = arrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)

    for(i in 0.. myArray.size - 2) { //изменил 1 на 0, чтобы не пропустить последний элемент
        var isSwapped = false
        for(j in 0 until myArray.size - 1 - i) {
            if(myArray[j] > myArray[j+1]) { //поменял знак "меньше" на "больше", чтобы задать порядок по возр
                val swap = myArray[j]
                myArray[j] = myArray[j+1]
                myArray[j+1] = swap
                isSwapped = true
            }
        }
        if (!isSwapped) break
    }

    for (item in myArray) {
        print("$item ")
    }
}
