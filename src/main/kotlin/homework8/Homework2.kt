package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println("Общее количество овощей: " + vegetableSet(userCart, vegetableSet))
    println("Сумма покупок: " + cartCostCounter(userCart, prices, discountSet, discountValue))
}

fun vegetableSet(userCart: MutableMap<String, Int>, vegetableSet: Set<String>): Int {
    var vegetableCounter = 0

    for (vegetable in vegetableSet) {
        vegetableCounter += userCart.getOrDefault(vegetable, 0)
    }

    return vegetableCounter
}

fun cartCostCounter(userCart: MutableMap<String, Int>, prices: MutableMap<String, Double>, discountSet: Set<String>, discountValue:Double): Double {
    val discountCoefficient = 1 - discountValue // пересчитаем размер скидки в коэффициент для умножения
    var total = 0.0
    for ((position, quantity) in userCart) {
        // использую getOrDefault, если на товар из корзины не окажется ценника
        if (discountSet.contains(position)) total += (prices.getOrDefault(position, 0.0) * discountCoefficient * quantity)
        else total += (prices.getOrDefault(position, 0.0) * quantity)
    }

    return total
}