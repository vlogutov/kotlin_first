package homework5

import kotlin.system.exitProcess

fun main() {
    //Ввод числа для первого старого преобразователя
    //Делаю через try-catch, чтобы поймать исключение на ввод не цифр
    //При вводе числа выходящего за диапазон 32 бит тоже срабатывает NumberFormatException
    var digit: Int = 0 //try catch подразумевает, что переменная будет создана до констуркции
    print("Введите целое число: ")
    try {
        digit = readln().toInt()
    } catch (e: NumberFormatException) {
        println("Внимание! Это слишком большое число или не число вовсе")
        exitProcess(0) //завершаю процесс с кодом 0 (нормальное завершение)
    }
    println(digitReverser(digit))

    //Ввод числа для модернизированной функции. Она преобразует всё, что ей придет на вход
    //Работает со строками
    print("Введите строку, которую нужно обратить: ")
    println(modernReverser(readln()))

}

fun digitReverser(digit: Int): Int {
    var tempDigit = digit //временоне число, чтобы не работать с исх переменной
    var newDigit: Int = 0 //новое число

    while (tempDigit != 0) {
        newDigit = newDigit * 10 + tempDigit % 10 //поднимаем пред. рез. на разряд выше и прибав. последний разряд числа
        tempDigit = tempDigit / 10
    }

    return newDigit
}

fun modernReverser(str: String): String {
    val strList = str.toList() //преобразую строку в список
    var reversedList = mutableListOf<Char>() //создаю пустой изменяемый список, чтобы перегнать в него исходный
    //var revStr: String = ""

    //перебираю исходный список с конца
    for (i in strList.size - 1 downTo 0) {
        //revStr = revStr + strList[i]
        reversedList.add(strList[i]) //перегоняю исходный список в новый
    }
    //нашел метод, который преобразует список в строку с возможностью кастомизации
    return reversedList.joinToString(separator = "") //меня интересует только сепаратор, остальное по умолчанию
}