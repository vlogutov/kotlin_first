package homework5

fun main() {
    var myLion = Lion("Alex", 200, 170)
    var myTiger = Tiger("Tigra", 190, 150)
    var myHippo = Hippopotamus("Hippo", 150, 300)
    var myWolf = Wolf("Secret", 170, 100)
    var myGiraffe = Giraffe("Melman", 250, 100)
    var myElephant = Elephant("Elly", 220, 700)
    var myChimpanzee = Chimpanzee("Chico", 150, 70)
    var myGorilla = Gorilla("Hocco", 185, 110)

    myLion.eat("meat")
    myTiger.eat("milk")
    myHippo.eat("grass")
    myWolf.eat("rabbit")
    myGiraffe.eat("potato")
    myElephant.eat("grass")
    myChimpanzee.eat("banana")
    myGorilla.eat("banana")


    //проверка работоспособности условия, что животное не есть такую еду
    //добавить этот код для льва и тигра
    myLion.eat("milk")
    myLion.eat("wine")
}

class Lion(val name: String, var height: Int, var weight: Int) {
    var fullness: Int = 0 //сытость, по умолчанию 0
    var foodPrefs = arrayOf("meat", "milk") // массив под предпочтения

    // Добавил второй конструктор с параметрами предпочтений и сытости
    // Это необходимо, чтобы можно было переназначить эти параметры при создании объекта
    constructor(name: String, height: Int, weight: Int, fullness: Int, foodPrefs: Array<String>): this(name, height, weight) {
        this.foodPrefs = foodPrefs
        this.fullness = fullness
    }

    fun eat(food: String) {
        var isFavFood: Boolean = false
        for (i in foodPrefs) {
            if (food == i) {
                fullness++
                isFavFood = true
                println("${this.name} съёл $food, сытость равна ${this.fullness}")
            }
        }
        if (!isFavFood) println("${this.name} не любит $food")
    }
}

class Tiger(val name: String, var height: Int, var weight: Int) {
    var fullness: Int = 0 //сытость, по умолчанию 0
    var foodPrefs = arrayOf("meat", "milk", "pork") //массив под предпочтения

    // Добавил второй конструктор с параметрами предпочтений и сытости
    // Это необходимо, чтобы можно было переназначить эти параметры при создании объекта
    constructor(name: String, height: Int, weight: Int, fullness: Int, foodPrefs: Array<String>): this(name, height, weight) {
        this.fullness = fullness
        this.foodPrefs = foodPrefs
    }

    fun eat(food: String) {
        var isFavFood: Boolean = false
        for (i in foodPrefs) {
            if (food == i) {
                fullness++
                isFavFood = true
                println("${this.name} съёл $food, сытость равна ${this.fullness}")
            }
        }
        if (!isFavFood) println("${this.name} не любит $food")
    }
}

class Hippopotamus(val name: String, var height: Int, var weight: Int) {
    var fullness: Int = 0 //сытость, по умолчанию 0
    var foodPrefs = arrayOf("grass", "milk") // массив под предпочтения

    // Добавил второй конструктор с параметрами предпочтений и сытости
    // Это необходимо, чтобы можно было переназначить эти параметры при создании объекта
    constructor(name: String, height: Int, weight: Int, fullness: Int, foodPrefs: Array<String>): this(name, height, weight) {
        this.fullness = fullness
        this.foodPrefs = foodPrefs
    }

    fun eat(food: String) {
        for (i in foodPrefs) {
            if (food == i) {
                fullness++
                println("${this.name} съёл $food, сытость равна ${this.fullness}")
            }
        }
    }
}

class Wolf(val name: String, var height: Int, var weight: Int) {
    var fullness: Int = 0 //сытость, по умолчанию 0
    var foodPrefs = arrayOf("rabbit", "squirrel") //массив под предпочтения

    // Добавил второй конструктор с параметрами предпочтений и сытости
    // Это необходимо, чтобы можно было переназначить эти параметры при создании объекта
    constructor(name: String, height: Int, weight: Int, fullness: Int, foodPrefs: Array<String>): this(name, height, weight) {
        this.fullness = fullness
        this.foodPrefs = foodPrefs
    }

    fun eat(food: String) {
        for (i in foodPrefs) {
            if (food == i) {
                fullness++
                println("${this.name} съёл $food, сытость равна ${this.fullness}")
            }
        }
    }
}

class Giraffe(val name: String, var height: Int, var weight: Int) {
    var fullness: Int = 0 //сытость, по умолчанию 0
    var foodPrefs = arrayOf("potato") //пустой массив под предпочтения

    // Добавил второй конструктор с параметрами предпочтений и сытости
    // Это необходимо, чтобы можно было переназначить эти параметры при создании объекта
    constructor(name: String, height: Int, weight: Int, fullness: Int, foodPrefs: Array<String>): this(name, height, weight) {
        this.fullness = fullness
        this.foodPrefs = foodPrefs
    }

    fun eat(food: String) {
        for (i in foodPrefs) {
            if (food == i) {
                fullness++
                println("${this.name} съёл $food, сытость равна ${this.fullness}")
            }
        }
    }
}

class Elephant(val name: String, var height: Int, var weight: Int) {
    var fullness: Int = 0 //сытость, по умолчанию 0
    var foodPrefs = arrayOf("grass") //массив под предпочтения

    // Добавил второй конструктор с параметрами предпочтений и сытости
    // Это необходимо, чтобы можно было переназначить эти параметры при создании объекта
    constructor(name: String, height: Int, weight: Int, fullness: Int, foodPrefs: Array<String>): this(name, height, weight) {
        this.fullness = fullness
        this.foodPrefs = foodPrefs
    }

    fun eat(food: String) {
        for (i in foodPrefs) {
            if (food == i) {
                fullness++
                println("${this.name} съёл $food, сытость равна ${this.fullness}")
            }
        }
    }
}

class Chimpanzee(val name: String, var height: Int, var weight: Int) {
    var fullness: Int = 0 //сытость, по умолчанию 0
    var foodPrefs = arrayOf("banana") // массив под предпочтения

    // Добавил второй конструктор с параметрами предпочтений и сытости
    // Это необходимо, чтобы можно было переназначить эти параметры при создании объекта
    constructor(name: String, height: Int, weight: Int, fullness: Int, foodPrefs: Array<String>): this(name, height, weight) {
        this.fullness = fullness
        this.foodPrefs = foodPrefs
    }

    fun eat(food: String) {
        for (i in foodPrefs) {
            if (food == i) {
                fullness++
                println("${this.name} съёл $food, сытость равна ${this.fullness}")
            }
        }
    }
}

class Gorilla(val name: String, var height: Int, var weight: Int) {
    var fullness: Int = 0 //сытость, по умолчанию 0
    var foodPrefs = arrayOf("banana") // массив под предпочтения

    // Добавил второй конструктор с параметрами предпочтений и сытости
    // Это необходимо, чтобы можно было переназначить эти параметры при создании объекта
    constructor(name: String, height: Int, weight: Int, fullness: Int, foodPrefs: Array<String>): this(name, height, weight) {
        this.fullness = fullness
        this.foodPrefs = foodPrefs
    }

    fun eat(food: String) {
        for (i in foodPrefs) {
            if (food == i) {
                fullness++
                println("${this.name} съёл $food, сытость равна ${this.fullness}")
            }
        }
    }
}