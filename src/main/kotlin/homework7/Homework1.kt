package homework7

fun main() {

    modernCreateUser()

/* Код для работы старого CreateUser закомментирован */
//    print("Введите логин (не более 20 знаков): ")
//    val login: String = readln()
//    print("Введите пароль (не менее 10 знаков): ")
//    val password: String = readln()
//    print("Повторите пароль: ")
//    val passConf: String = readln()
//
//    val newUser = createUser(login, password, passConf)
//    println("Логин нового пользователя: " + newUser.login)
//    println("Пароль нового пользователя: " + newUser.password)
}

// Новый метод создания пользователя, который делает проверки налету и переспрашивает в случае ошибки
fun modernCreateUser(): User {
    val login = getLogin()
    val password = getPassword()

    println("Пользователь \"$login\" создан. Пароль $password")
    return User(login, password)
}


// Функция спрашивает логин. И бесконечно переспрашивает его, пока он не будет соответствовать условию
// Заход в while происходит только если первый введенный логин не подходит под условие
fun getLogin(): String {
    print("Введите логин (не более 20 знаков): ")
    var login = readln()
    while (login.length > 20) {
        print("Вы ввели неподходящий логин. Повторите ввод (не более 20 знаков): ")
        login = readln()
    }
    return login
}

// Функция спрашивает пароль. И бесконечно переспрашивает его, пока он не будет соответствовать условию
fun passwordVerificator(): String {
    print("Введите пароль (не менее 10 знаков): ")
    var password = readln()
    while (password.length < 10) {
        print("Вы ввели неправильный пароль. Введите ещё раз не менее 10 знаков: ")
        password = readln()
    }
    return password
}

// Функция принимает на вход пароль. Спрашивает подтверждение, сранивает их и возвращает true/false
fun confirmationVerificator(password: String): Boolean {
    print("Подтвердите пароль: ")
    val confirmation = readln()
    if (password == confirmation) return true
    else return false
}

// Функция вызывает passwordVerificator и confirmationVerificator
// Если они не совпадают, то функция повторно вызывает passwordVerificator и confirmationVerificator
// Такая логика необходима, чтобы при неправильном подтверждении спрашивать пароль с заново
// Логику confirmationVerificator можно упаковать в getPassword, но будет нелогичная архитектура
fun getPassword(): String {
    var password = passwordVerificator()
    var passConfirmed = confirmationVerificator(password)

    while(!passConfirmed) {
        println("Пароль и подтверждение не совпали. Введите пароль заново!")
        password = passwordVerificator()
        passConfirmed = confirmationVerificator(password)
    }
    return password
}


//Старый метод createUser, который вызывает исключения
fun createUser(login: String, password: String, passConf: String): User {

    if(login.length > 20) {
        throw WrongLoginException("Длина логина больше 20. Длина = ${login.length}")
    }
    else if (password.length < 10 || password != passConf) {
        throw WrongPasswordException("Пароль меньше 10 символов или не совпадает с подтверждением")
    }

    return User(login, password)
}

class WrongLoginException(excMessage: String): Exception(excMessage)

class WrongPasswordException(excMessage: String): Exception(excMessage)

class User(val login: String, val password: String)
